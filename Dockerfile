FROM python:alpine3.19 

WORKDIR /app

COPY main.py /app/

ENTRYPOINT ["python3", "main.py"]
